-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: app_voc
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.12-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Creation d'un user api
--


CREATE USER 'api'@'localhost';
ALTER USER 'api'@'localhost'
IDENTIFIED BY 'admin' ;
GRANT Usage ON *.* TO 'api'@'localhost';
GRANT Update ON api_voc.* TO 'api'@'localhost';
GRANT Select ON api_voc.* TO 'api'@'localhost';
FLUSH PRIVILEGES;



--
-- Table structure for table `cartes`
--

DROP TABLE IF EXISTS `cartes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mot` int(10) NOT NULL,
  `traduction` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_carte_mot` (`mot`),
  KEY `FK_carte_traduction` (`traduction`),
  CONSTRAINT `FK_carte_mot` FOREIGN KEY (`mot`) REFERENCES `mots` (`id`),
  CONSTRAINT `FK_carte_traduction` FOREIGN KEY (`traduction`) REFERENCES `mots` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `cartes_categories`
--

DROP TABLE IF EXISTS `cartes_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartes_categories` (
  `idCarte` int(11) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  PRIMARY KEY (`idCarte`,`idCategorie`),
  KEY `FK_carteCategorie_categorie` (`idCategorie`),
  CONSTRAINT `FK_carteCategorie_carte` FOREIGN KEY (`idCarte`) REFERENCES `cartes` (`id`),
  CONSTRAINT `FK_carteCategorie_categorie` FOREIGN KEY (`idCategorie`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `compartiments`
--

DROP TABLE IF EXISTS `compartiments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compartiments` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `compartiments_users`
--

DROP TABLE IF EXISTS `compartiments_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compartiments_users` (
  `idUser` int(11) NOT NULL,
  `idCarte` int(11) NOT NULL,
  `idCompartiment` int(5) NOT NULL,
  PRIMARY KEY (`idUser`,`idCarte`),
  KEY `FK_compartimentUser_carte` (`idCarte`),
  KEY `FK_compartimentUser_compartiment` (`idCompartiment`),
  CONSTRAINT `FK_compartimentUser_carte` FOREIGN KEY (`idCarte`) REFERENCES `cartes` (`id`),
  CONSTRAINT `FK_compartimentUser_compartiment` FOREIGN KEY (`idCompartiment`) REFERENCES `compartiments` (`id`),
  CONSTRAINT `FK_compartimentUser_user` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `langues`
--

DROP TABLE IF EXISTS `langues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `langues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `mots`
--

DROP TABLE IF EXISTS `mots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLangue` int(11) NOT NULL,
  `libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mot_langue` (`idLangue`),
  CONSTRAINT `FK_mot_langue` FOREIGN KEY (`idLangue`) REFERENCES `langues` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `token` varchar(100) DEFAULT NULL,
  `token_validity` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nomPrenomEmail` (`nom`,`prenom`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Dumping routines for database 'app_voc'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-17 15:48:46
