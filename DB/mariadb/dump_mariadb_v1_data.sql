-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: app_voc
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.12-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Creation d'un user api
--


CREATE USER 'api'@'localhost';
ALTER USER 'api'@'localhost'
IDENTIFIED BY 'admin' ;
GRANT Usage ON *.* TO 'api'@'localhost';
GRANT Update ON api_voc.* TO 'api'@'localhost';
GRANT Select ON api_voc.* TO 'api'@'localhost';
FLUSH PRIVILEGES;



--
-- Table structure for table `cartes`
--

DROP TABLE IF EXISTS `cartes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mot` int(10) NOT NULL,
  `traduction` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_carte_mot` (`mot`),
  KEY `FK_carte_traduction` (`traduction`),
  CONSTRAINT `FK_carte_mot` FOREIGN KEY (`mot`) REFERENCES `mots` (`id`),
  CONSTRAINT `FK_carte_traduction` FOREIGN KEY (`traduction`) REFERENCES `mots` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartes`
--

LOCK TABLES `cartes` WRITE;
/*!40000 ALTER TABLE `cartes` DISABLE KEYS */;
INSERT INTO `cartes` VALUES (1,1,26),(2,2,27),(3,3,28),(4,4,29),(5,5,30),(6,6,31),(7,7,32),(8,8,33),(9,9,34),(10,10,35),(11,11,36),(12,12,37),(13,13,38),(14,14,39),(15,15,40),(16,16,41),(17,17,42),(18,18,43),(19,19,44),(20,20,45),(21,21,46),(22,22,47),(23,23,48),(24,24,49),(25,25,50);
/*!40000 ALTER TABLE `cartes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cartes_categories`
--

DROP TABLE IF EXISTS `cartes_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartes_categories` (
  `idCarte` int(11) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  PRIMARY KEY (`idCarte`,`idCategorie`),
  KEY `FK_carteCategorie_categorie` (`idCategorie`),
  CONSTRAINT `FK_carteCategorie_carte` FOREIGN KEY (`idCarte`) REFERENCES `cartes` (`id`),
  CONSTRAINT `FK_carteCategorie_categorie` FOREIGN KEY (`idCategorie`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartes_categories`
--

LOCK TABLES `cartes_categories` WRITE;
/*!40000 ALTER TABLE `cartes_categories` DISABLE KEYS */;
INSERT INTO `cartes_categories` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,2);
/*!40000 ALTER TABLE `cartes_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'animaux'),(2,'voyage');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compartiments`
--

DROP TABLE IF EXISTS `compartiments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compartiments` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compartiments`
--

LOCK TABLES `compartiments` WRITE;
/*!40000 ALTER TABLE `compartiments` DISABLE KEYS */;
INSERT INTO `compartiments` VALUES (1,'compartiment_1'),(2,'compartiment_2'),(3,'compartiment_3'),(4,'compartiment_4'),(5,'compartiment_5');
/*!40000 ALTER TABLE `compartiments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compartiments_users`
--

DROP TABLE IF EXISTS `compartiments_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compartiments_users` (
  `idUser` int(11) NOT NULL,
  `idCarte` int(11) NOT NULL,
  `idCompartiment` int(5) NOT NULL,
  PRIMARY KEY (`idUser`,`idCarte`),
  KEY `FK_compartimentUser_carte` (`idCarte`),
  KEY `FK_compartimentUser_compartiment` (`idCompartiment`),
  CONSTRAINT `FK_compartimentUser_carte` FOREIGN KEY (`idCarte`) REFERENCES `cartes` (`id`),
  CONSTRAINT `FK_compartimentUser_compartiment` FOREIGN KEY (`idCompartiment`) REFERENCES `compartiments` (`id`),
  CONSTRAINT `FK_compartimentUser_user` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compartiments_users`
--

LOCK TABLES `compartiments_users` WRITE;
/*!40000 ALTER TABLE `compartiments_users` DISABLE KEYS */;
INSERT INTO `compartiments_users` VALUES (1,13,1),(1,14,1),(1,10,2),(1,11,2),(1,12,2),(3,1,2),(3,2,2),(3,3,2),(3,4,2),(3,5,2),(3,6,2),(3,7,2),(3,8,2),(3,9,2),(3,10,2),(3,11,2),(3,12,2),(3,13,2),(3,14,2),(3,15,2),(3,16,2),(3,17,2),(3,18,2),(3,19,2),(3,20,2),(3,21,2),(3,22,2),(3,23,2),(1,1,3),(1,5,3),(1,9,3),(1,17,3),(1,18,3),(1,19,3),(1,20,3),(1,21,3),(1,22,3),(1,23,3),(1,24,3),(1,25,3),(2,2,3),(2,3,3),(2,4,3),(2,5,3),(2,6,3),(2,7,3),(2,8,3),(2,9,3),(2,10,3),(2,11,3),(2,12,3),(2,13,3),(2,14,3),(2,15,3),(2,16,3),(2,17,3),(2,18,3),(2,19,3),(2,20,3),(2,21,3),(2,22,3),(2,23,3),(2,24,3),(3,24,3),(1,6,4),(1,7,4),(2,1,4),(1,2,5),(1,3,5),(1,4,5),(1,8,5),(1,15,5),(1,16,5),(2,25,5),(3,25,5);
/*!40000 ALTER TABLE `compartiments_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `langues`
--

DROP TABLE IF EXISTS `langues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `langues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `langues`
--

LOCK TABLES `langues` WRITE;
/*!40000 ALTER TABLE `langues` DISABLE KEYS */;
INSERT INTO `langues` VALUES (1,'français'),(2,'anglais'),(3,'allemand'),(4,'espagnol');
/*!40000 ALTER TABLE `langues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mots`
--

DROP TABLE IF EXISTS `mots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLangue` int(11) NOT NULL,
  `libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mot_langue` (`idLangue`),
  CONSTRAINT `FK_mot_langue` FOREIGN KEY (`idLangue`) REFERENCES `langues` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mots`
--

LOCK TABLES `mots` WRITE;
/*!40000 ALTER TABLE `mots` DISABLE KEYS */;
INSERT INTO `mots` VALUES (1,1,'chien'),(2,1,'chat'),(3,1,'cheval'),(4,1,'serpent'),(5,1,'cochon'),(6,1,'oiseau'),(7,1,'poisson'),(8,1,'arraignée'),(9,1,'singe'),(10,1,'lapin'),(11,1,'our'),(12,1,'phoque'),(13,1,'morse'),(14,1,'tigre'),(15,1,'zèbre'),(16,1,'vache'),(17,1,'cheval'),(18,1,'canard'),(19,1,'poule'),(20,1,'coq'),(21,1,'chevre'),(22,1,'mouton'),(23,1,'raie'),(24,1,'tortue'),(25,1,'dauphin'),(26,2,'dog'),(27,2,'cat'),(28,2,'horse'),(29,2,'snake'),(30,2,'pig'),(31,2,'bird'),(32,2,'fish'),(33,2,'spider'),(34,2,'monkey'),(35,2,'rabbit'),(36,2,'bear'),(37,2,'seal'),(38,2,'walrus'),(39,2,'tiger'),(40,2,'zebra'),(41,2,'cow'),(42,2,'horse'),(43,2,'duck'),(44,2,'hen'),(45,2,'cock'),(46,2,'goat'),(47,2,'sheep'),(48,2,'skate'),(49,2,'turtle'),(50,3,'dolphin');
/*!40000 ALTER TABLE `mots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `token` varchar(100) DEFAULT NULL,
  `token_validity` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nomPrenomEmail` (`nom`,`prenom`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'taboada','lionel','lionel@rpn.ch','5befb0748e190886505727a102475cc24fbaa116','0059dd23-2671-4799-8170-7b8698d56f48','2019-09-12 16:20:41'),(2,'fogale','marco','marco@rpn.ch','3829486b93ec44395f0b980424bae9b6fb07b7bc','f400c754-25a0-4b74-b1b1-6b06e88bb8a8','2019-09-12 16:20:53'),(3,'lochmatter','yaimara','yaimara@rpn.ch','a5ad27663aa0dc04c62e4041fd38c17e12bfe127','2c3af240-5fcd-491f-b8a9-e34ac1dc84ac','2019-09-12 15:45:08');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'app_voc'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-17 15:48:46
