--
--SQL CREATION SCRIPTS FOR API VOCABULAIRE
-- 
-- ------------------------------------------------------
-- PSQL

--****************
-- Set path for admin user with the schema api created below
--****************

set search_path = "$user", public, api;

--****************
-- Create user api
--****************

--CREATION D'UN USER ET SCHEMA API
CREATE ROLE api LOGIN NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE encrypted password 'admin';
CREATE SCHEMA IF NOT EXISTS api;


--*************
-- Create tables dans le schema api.
--*************


--
-- Table structure for table users
--

DROP TABLE IF EXISTS api.users;

CREATE TABLE api.users (
  id serial PRIMARY KEY,
  nom varchar(25) NOT NULL,
  prenom varchar(25) NOT NULL,
  email varchar(25) NOT NULL ,
  password varchar(100) NOT NULL,
  token varchar(100) ,
  token_validity timestamp 
  
);

--
-- Table structure for table cartes 
--

DROP TABLE IF EXISTS api.cartes;

CREATE TABLE api.cartes (
  id serial PRIMARY KEY,
  mot INTEGER NOT NULL,
  traduction INTEGER NOT NULL
) ;


--
-- Table structure for table cartes_categories
--

DROP TABLE IF EXISTS api.cartes_categories;

CREATE TABLE api.cartes_categories (
  idCarte INTEGER NOT NULL,
  idCategorie INTEGER NOT NULL,
  PRIMARY KEY (idCarte,idCategorie)
);

--
-- Table structure for table categories 
--

DROP TABLE IF EXISTS api.categories;

CREATE TABLE api.categories (
 id serial PRIMARY KEY,
  libelle varchar(25) NOT NULL
);

--
-- Table structure for table compartiments 
--

DROP TABLE IF EXISTS api.compartiments;

CREATE TABLE api.compartiments (
  id serial PRIMARY KEY,
  libelle varchar(25) NOT NULL
);

--
-- Table structure for table langues 
--

DROP TABLE IF EXISTS api.langues;

CREATE TABLE api.langues (
  id serial PRIMARY KEY,
  libelle varchar(25) NOT NULL
);


--
-- Table structure for table mots 
--

DROP TABLE IF EXISTS api.mots;

CREATE TABLE api.mots (
  id serial PRIMARY KEY,
  idLangue INTEGER NOT NULL,
  libelle varchar(25) NOT NULL

);

--
-- Table structure for table compartiments_users 
--

DROP TABLE IF EXISTS api.compartiments_users;

CREATE TABLE api.compartiments_users (
  idUser INTEGER NOT NULL,
  idCarte INTEGER NOT NULL,
  idCompartiment INTEGER NOT NULL,
  PRIMARY KEY (idUser,idCarte)
) ;



--*************
-- Dumping data
--*************

--
-- Dumping data for table users 
--

INSERT INTO api.users VALUES (1,'taboada','lionel','lionel@rpn.ch','5befb0748e190886505727a102475cc24fbaa116','318010210345001543834138981650058009948014131366910187477021','2019-09-06 11:55:32'),(2,'fogale','marco','marco@rpn.ch','3829486b93ec44395f0b980424bae9b6fb07b7bc','782841196578790472621694977821410729901111166714756908545699','2019-09-06 10:18:29'),(3,'lochmatter','yaimara','yaimara@rpn.ch','a5ad27663aa0dc04c62e4041fd38c17e12bfe127','170147966887978702603191340645969336828146464192885216215212','2019-09-05 21:15:38');

--
-- Dumping data for table cartes 
--

INSERT INTO api.cartes VALUES (1,1,26),(2,2,27),(3,3,28),(4,4,29),(5,5,30),(6,6,31),(7,7,32),(8,8,33),(9,9,34),(10,10,35),(11,11,36),(12,12,37),(13,13,38),(14,14,39),(15,15,40),(16,16,41),(17,17,42),(18,18,43),(19,19,44),(20,20,45),(21,21,46),(22,22,47),(23,23,48),(24,24,49),(25,25,50);

--
-- Dumping data for table cartes_categories
--

INSERT INTO api.cartes_categories VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,2);

--
-- Dumping data for table categories
--

INSERT INTO api.categories VALUES (1,'animaux'),(2,'voyage');


--
-- Dumping data for table compartiments
--

INSERT INTO api.compartiments (libelle) VALUES ('compartiment_1'),('compartiment_2'),('compartiment_3'),('compartiment_4'),('compartiment_5');


--
-- Dumping data for table langues
--

INSERT INTO api.langues (libelle) VALUES ('français'),('anglais'),('allemand'),('espagnol');

--
-- Dumping data for table mots
--

INSERT INTO api.mots VALUES (1,1,'chien'),(2,1,'chat'),(3,1,'cheval'),(4,1,'serpent'),(5,1,'cochon'),(6,1,'oiseau'),(7,1,'poisson'),(8,1,'arraignée'),(9,1,'singe'),(10,1,'lapin'),(11,1,'our'),(12,1,'phoque'),(13,1,'morse'),(14,1,'tigre'),(15,1,'zèbre'),(16,1,'vache'),(17,1,'cheval'),(18,1,'canard'),(19,1,'poule'),(20,1,'coq'),(21,1,'chevre'),(22,1,'mouton'),(23,1,'raie'),(24,1,'tortue'),(25,1,'dauphin'),(26,2,'dog'),(27,2,'cat'),(28,2,'horse'),(29,2,'snake'),(30,2,'pig'),(31,2,'bird'),(32,2,'fish'),(33,2,'spider'),(34,2,'monkey'),(35,2,'rabbit'),(36,2,'bear'),(37,2,'seal'),(38,2,'walrus'),(39,2,'tiger'),(40,2,'zebra'),(41,2,'cow'),(42,2,'horse'),(43,2,'duck'),(44,2,'hen'),(45,2,'cock'),(46,2,'goat'),(47,2,'sheep'),(48,2,'skate'),(49,2,'turtle'),(50,3,'dolphin');


--
-- Dumping data for table compartiments_users 
--

INSERT INTO api.compartiments_users VALUES (1,13,1),(1,14,1),(1,10,2),(1,11,2),(1,12,2),(3,1,2),(3,2,2),(3,3,2),(3,4,2),(3,5,2),(3,6,2),(3,7,2),(3,8,2),(3,9,2),(3,10,2),(3,11,2),(3,12,2),(3,13,2),(3,14,2),(3,15,2),(3,16,2),(3,17,2),(3,18,2),(3,19,2),(3,20,2),(3,21,2),(3,22,2),(3,23,2),(1,1,3),(1,5,3),(1,9,3),(1,17,3),(1,18,3),(1,19,3),(1,20,3),(1,21,3),(1,22,3),(1,23,3),(1,24,3),(1,25,3),(2,2,3),(2,3,3),(2,4,3),(2,5,3),(2,6,3),(2,7,3),(2,8,3),(2,9,3),(2,10,3),(2,11,3),(2,12,3),(2,13,3),(2,14,3),(2,15,3),(2,16,3),(2,17,3),(2,18,3),(2,19,3),(2,20,3),(2,21,3),(2,22,3),(2,23,3),(2,24,3),(3,24,3),(1,6,4),(1,7,4),(1,8,4),(2,1,4),(1,2,5),(1,3,5),(1,4,5),(1,15,5),(1,16,5),(2,25,5),(3,25,5);



--**************************
-- Constraint section
--**************************

--
-- Constraint for table users
--

ALTER TABLE api.users	 
ADD CONSTRAINT XU_personne_NomPrenomEmail
	UNIQUE (nom,prenom,email);



--
-- Constraint for table cartes
--

ALTER TABLE api.cartes
ADD CONSTRAINT FK_carte_mot FOREIGN KEY (mot) REFERENCES api.mots (id),
ADD CONSTRAINT FK_carte_traduction FOREIGN KEY (traduction) REFERENCES api.mots (id);


--
-- Constraint for table cartes_categories
--
ALTER TABLE api.cartes_categories
ADD CONSTRAINT FK_carteCategorie_carte FOREIGN KEY (idCarte) REFERENCES api.cartes (id),
ADD CONSTRAINT FK_carteCategorie_categorie FOREIGN KEY (idCategorie) REFERENCES api.categories (id);


--
-- Constraint for table mots
--
ALTER TABLE api.mots
ADD CONSTRAINT FK_mot_langue FOREIGN KEY (idLangue) REFERENCES api.langues (id);


--
-- Constraint for table compartiments_users
--

ALTER TABLE api.compartiments_users
ADD CONSTRAINT FK_compartimentUser_carte FOREIGN KEY (idCarte) REFERENCES api.cartes (id),
ADD CONSTRAINT FK_compartimentUser_compartiment FOREIGN KEY (idCompartiment) REFERENCES api.compartiments (id),
ADD CONSTRAINT FK_compartimentUser_user FOREIGN KEY (idUser) REFERENCES api.users (id);



--DONNE LES DROITS EN LECTURE ET MODIFICATION AU USER api SUR LE SCHEMA api
grant usage on schema api to api;
REVOKE CREATE ON SCHEMA api FROM api;
REVOKE INSERT, DELETE ON ALL TABLES IN SCHEMA api FROM api;
GRANT SELECT, UPDATE ON ALL TABLES IN SCHEMA api TO api;

