--
--SQL CREATION SCRIPTS FOR API VOCABULAIRE
-- 
-- ------------------------------------------------------
-- PSQL

--****************
-- Set path for admin user with the schema api created below
--****************

set search_path = "$user", public, api;

--****************
-- Create user api
--****************

--CREATION D'UN USER ET SCHEMA API
CREATE ROLE api LOGIN NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE encrypted password 'admin';
CREATE SCHEMA IF NOT EXISTS api;


--*************
-- Create tables dans le schema api.
--*************


--
-- Table structure for table users
--

DROP TABLE IF EXISTS api.users;

CREATE TABLE api.users (
  id serial PRIMARY KEY,
  nom varchar(25) NOT NULL,
  prenom varchar(25) NOT NULL,
  email varchar(25) NOT NULL ,
  password varchar(100) NOT NULL,
  token varchar(100) ,
  token_validity timestamp 
  
);

--
-- Table structure for table cartes 
--

DROP TABLE IF EXISTS api.cartes;

CREATE TABLE api.cartes (
  id serial PRIMARY KEY,
  mot INTEGER NOT NULL,
  traduction INTEGER NOT NULL
) ;


--
-- Table structure for table cartes_categories
--

DROP TABLE IF EXISTS api.cartes_categories;

CREATE TABLE api.cartes_categories (
  idCarte INTEGER NOT NULL,
  idCategorie INTEGER NOT NULL,
  PRIMARY KEY (idCarte,idCategorie)
);

--
-- Table structure for table categories 
--

DROP TABLE IF EXISTS api.categories;

CREATE TABLE api.categories (
 id serial PRIMARY KEY,
  libelle varchar(25) NOT NULL
);

--
-- Table structure for table compartiments 
--

DROP TABLE IF EXISTS api.compartiments;

CREATE TABLE api.compartiments (
  id serial PRIMARY KEY,
  libelle varchar(25) NOT NULL
);

--
-- Table structure for table langues 
--

DROP TABLE IF EXISTS api.langues;

CREATE TABLE api.langues (
  id serial PRIMARY KEY,
  libelle varchar(25) NOT NULL
);


--
-- Table structure for table mots 
--

DROP TABLE IF EXISTS api.mots;

CREATE TABLE api.mots (
  id serial PRIMARY KEY,
  idLangue INTEGER NOT NULL,
  libelle varchar(25) NOT NULL

);

--
-- Table structure for table compartiments_users 
--

DROP TABLE IF EXISTS api.compartiments_users;

CREATE TABLE api.compartiments_users (
  idUser INTEGER NOT NULL,
  idCarte INTEGER NOT NULL,
  idCompartiment INTEGER NOT NULL,
  PRIMARY KEY (idUser,idCarte)
) ;


--**************************
-- Constraint section
--**************************


--
-- Constraint for table users
--

ALTER TABLE api.users	 
ADD CONSTRAINT XU_personne_NomPrenomEmail
	UNIQUE (nom,prenom,email);



--
-- Constraint for table cartes
--

ALTER TABLE api.cartes
ADD CONSTRAINT FK_carte_mot FOREIGN KEY (mot) REFERENCES api.mots (id),
ADD CONSTRAINT FK_carte_traduction FOREIGN KEY (traduction) REFERENCES api.mots (id);


--
-- Constraint for table cartes_categories
--
ALTER TABLE api.cartes_categories
ADD CONSTRAINT FK_carteCategorie_carte FOREIGN KEY (idCarte) REFERENCES api.cartes (id),
ADD CONSTRAINT FK_carteCategorie_categorie FOREIGN KEY (idCategorie) REFERENCES api.categories (id);


--
-- Constraint for table mots
--
ALTER TABLE api.mots
ADD CONSTRAINT FK_mot_langue FOREIGN KEY (idLangue) REFERENCES api.langues (id);


--
-- Constraint for table compartiments_users
--

ALTER TABLE api.compartiments_users
ADD CONSTRAINT FK_compartimentUser_carte FOREIGN KEY (idCarte) REFERENCES api.cartes (id),
ADD CONSTRAINT FK_compartimentUser_compartiment FOREIGN KEY (idCompartiment) REFERENCES api.compartiments (id),
ADD CONSTRAINT FK_compartimentUser_user FOREIGN KEY (idUser) REFERENCES api.users (id);



--DONNE LES DROITS EN LECTURE ET MODIFICATION AU USER api SUR LE SCHEMA api
grant usage on schema api to api;
REVOKE CREATE ON SCHEMA api FROM api;
REVOKE INSERT, DELETE ON ALL TABLES IN SCHEMA api FROM api;
GRANT SELECT, UPDATE ON ALL TABLES IN SCHEMA api TO api;

