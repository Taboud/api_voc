# API - vocabulaire

## Description
Cette API fournie des cartes de vocabulaire qui facilitent l'apprentissage d'une langue étrangère.

Chaque carte contient un mot en français et sa traduction dans une langue étrangère.

Cette API s'insipire de la méthode d'apprentissage "flashcards".

[Accèder au wiki du projet](https://gitlab.com/Taboud/api_voc/wikis/home)

