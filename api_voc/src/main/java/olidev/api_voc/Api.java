/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olidev.api_voc;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.simple.JSONArray;

/**
 *
 * @author lionel
 */
public class Api {

    /**
     * @param args the command line arguments
     */
    public static void main ( String[] args ) {

        
      
	/* --------------------
	 * VERTX CONFIGURATION
	 ---------------------- */
	Vertx vertx = Vertx.vertx ();
	Router router = Router.router ( vertx );
	//TO HANDLE THE JSON FROM A POST REQUEST
	router.route ().handler ( BodyHandler.create () );
        
        /** ------------------------
	 * HTTP SERVER CONFIGURATION
	 -------------------------- */
	HttpServer server = vertx.createHttpServer ();
	server.requestHandler ( router ).listen ( 8080 );
        
	/* ---------------
	 * ROUTE DE LOGIN
	 ---------------- */
	router.post ( "/login" ).handler ( ctx -> {

	    //Server HTTP
	    HttpServerResponse response = ctx.response ();
	    response.putHeader ( "Content-Type" , "application/json; charset=utf-8" );

	    //JSON OBJECT FOR RESPONSE 
	    JsonObject json = new JsonObject ();
            
            //CHECK IF THE REQUEST CONTAIN A JSON OBJECT WITH "USER" AND "PASSWORD"
	    if (  ! ctx.getBodyAsJson ().containsKey ( "user" ) ||  ! ctx.getBodyAsJson ().containsKey ( "password" ) ) {
		json.put ( "authoristion_status" , "denied" );
		json.put ( "SQL_status" , "ERROR : one or more JSON data(s)( are missing or false" );
		response.end ( json.encodePrettily () );
	    } else {
		// GET THE USER + PASSWORD ENCRYPTED IN SHA1
		String user = ctx.getBodyAsJson ().getString ( "user" );
		String password = Utils.sha1 ( ctx.getBodyAsJson ().getString ( "password" ) );

		
		try {

		    //CHECK USER & PASSWORD ARE IN THE DB
		    PreparedStatement st = DataBase.getConnection ().prepareStatement ( "select id from users where prenom = ? and password= ? " );
		    st.setString ( 1 , user );
		    st.setString ( 2 , password );
		    ResultSet res = st.executeQuery ();

		    //IF THE USER AND PASSWORD ARE CORRECT -> TOKEN CREATION
		    if ( res.next () ) {

			//TOKEN CREATION
			UUID uuid = UUID.randomUUID ();
			String token = uuid.toString ();

			//TOKEN VALIDITY PLUS 1 DAY FROM NOW
			Date currentDate = new Date ();
			LocalDateTime tokenValidity = currentDate.toInstant ().atZone ( ZoneId.systemDefault () ).toLocalDateTime ();
			tokenValidity = tokenValidity.plusDays ( 1 );

			//UPDATE USER TOKEN
			PreparedStatement st2 = DataBase.getConnection ().prepareStatement ( "update users set token= ?, token_validity= ? where id= ? " );
			st2.setString ( 1 , token.toString () );
			st2.setTimestamp ( 2 , java.sql.Timestamp.valueOf ( tokenValidity ) );
			st2.setInt ( 3 , res.getInt ( 1 ) );
			st2.executeUpdate ();

			//SEND THE TOKEN TO THE CLIENT
			json.put ( "authoristion_status" , "OK" );
			json.put ( "token_status" , "OK" );
			json.put ( "tokken" , token.toString () );
			response.end ( json.encodePrettily () );
                        
                        //CLOSE SQL STATEMENT
                        st2.close();
                        
                        

		    } else {
			json.put ( "authoristion_status" , "denied" );
			response.end ( json.encodePrettily () );

		    }
                    
                     //CLOSE SQL STATEMENT
                     st.close();
                     res.close();

		} catch ( Exception e ) {
		    e.printStackTrace ();
		}

	    }

	} );

	/*-------------------------------------------------
	 * ROUTE D'AUTORISATION PAR DEFAUT CONTROLE DU TOKEN
	 --------------------------------------------------*/
	router.route ().path ( "/api/*" ).handler ( ctx -> {

	    //Server HTTP
	    HttpServerResponse response = ctx.response ();

	    //JSON OBJECT RESPONSE
	    JsonObject json = new JsonObject ();
            
            //CHECK IF THE AUTHORIZATION PARMETERE IS IN THE REQUEST
	    if ( ctx.request ().getHeader ( "Authorization" ) == null ) {
		response.putHeader ( "Content-Type" , "application/json; charset=utf-8" );
		json.put ( "authoristion_status" , "denied" );
		response.end ( json.encodePrettily () );
	    } else {

		//GET THE TOKEN WHITOUT THE BEAR DESCRIPTION
		String tokenProvided = ctx.request ().getHeader ( "Authorization" ).substring ( 7 );

		
		try {

		    //GET THE USER AND TOKEN_VALIDITY RELATED TO THE TOKEN
		    PreparedStatement st = DataBase.getConnection ().prepareStatement ( "select id, token_validity from users where token = ? " );
		    st.setString ( 1 , tokenProvided );
		    ResultSet res = st.executeQuery ();
                    
                    //CHECK IF  THE TOKEN IS CORRECT
		    if ( res.next () ) {
			//GET THE TOKEN VALIDITY INFOS AND COMPARE IT TO THE CURRENT DATE
			String tokenValidity = res.getString ( 2 );
			Date tokenDateValidity = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss" ).parse ( tokenValidity );
			Date currentDate = new Date ();
                        
                        //CHECK TOKEN VALIDITY
			if ( tokenDateValidity.before ( currentDate ) ) {
			    response.putHeader ( "Content-Type" , "application/json; charset=utf-8" );
			    response.end ( new JsonObject ().put ( "authoristion_status" , "OK" ).put ( "token_status" , "expired" ).encodePrettily () );
			} else {
			    //PUT THE USER ID RELATED TO THE TOKEN INTO THE CONTEXT ROUTE
			    ctx.put ( "idUser" , res.getInt ( 1 ) );
			    ctx.put ( "authoristion_status" , "OK" );
			    ctx.put ( "token_status" , "OK" );
			    //PASS TO THE NEXT HANDLER
			    ctx.next ();
			}
		    } else {
			response.putHeader ( "Content-Type" , "application/json; charset=utf-8" );
			response.end ( new JsonObject ().put ( "authoristion_status" , "OK" ).put ( "token_status" , "matching error" ).encodePrettily () );
		    }
                    
                    //CLOSE SQL STATEMENT
                    st.close();
                    res.close();

		} catch ( Exception e ) {
		    e.printStackTrace ();
		}

	    }
            
	} );

	/**-----------------
	 * ROUTE LANGUE
	 ----------------- */
	router.get ( "/api/langues" ).handler ( ctx -> {

	    //Server HTTP
	    HttpServerResponse response = ctx.response ();
	    response.putHeader ( "Content-Type" , "application/json; charset=utf-8" );

	    //JSON utils
	    JsonObject jsonObject = new JsonObject ();
	    JSONArray arrJsonTemp = new JSONArray ();

	    //PUT AUTORISATION ET TOKEN STATUS IN THE JSON OBJECT
	    jsonObject.put ( "authoristion_status" , ctx.data ().get ( "authoristion_status" ).toString () );
	    jsonObject.put ( "token_status" , ctx.data ().get ( "token_status" ).toString () );

	    try {

		//GET ALL LANGUAGES FOR WHICH THERE IS A CARD, EXCEPT FOR THE FRENCH
		Statement st = DataBase.getConnection ().createStatement ();
		ResultSet res = st.executeQuery ( "select distinct langues.libelle from cartes "
			+ "inner join mots on cartes.traduction = mots.id "
			+ "inner join langues on langues.id = mots.idLangue "
			+ "where langues.libelle != 'français'" );

		//LOOP THROUGH DATA AND ADD IT TO JSON
		while ( res.next () ) {
		    arrJsonTemp.add ( res.getString ( 1 ) );
		}
                
                //CLOSE SQL STATEMENT
                st.close();
                res.close();


	    } catch ( Exception e ) {
		e.printStackTrace ();
	    }
	    //SQL STATUS
	    jsonObject.put ( "SQL_status" , "OK" );

	    //ADD DATA TO JSON OBJECT
	    jsonObject.put ( "data" , arrJsonTemp );

	    //DISPLAY RESULT IN JSON FORMAT
	    response.end ( jsonObject.encodePrettily () );

	} );
        
        /**-----------------
	 * ROUTE CATEGORIES
	 ----------------- */
	router.get ( "/api/categories" ).handler ( ctx -> {

	    //Server HTTP
	    HttpServerResponse response = ctx.response ();
	    response.putHeader ( "Content-Type" , "application/json; charset=utf-8" );

	    //JSON utils
	    JsonObject jsonObject = new JsonObject ();
	    JSONArray arrJsonTemp = new JSONArray ();

	    //PUT AUTORISATION ET TOKEN STATUS IN THE JSON OBJECT
	    jsonObject.put ( "authoristion_status" , ctx.data ().get ( "authoristion_status" ).toString () );
	    jsonObject.put ( "token_status" , ctx.data ().get ( "token_status" ).toString () );

	    try {

		//GET ALL THE CATEGORIES BY LANGUAGE EXCEPT FOR THE FRENCH
		PreparedStatement st = DataBase.getConnection ().prepareStatement ( "select distinct langues.libelle from cartes "
			+ "inner join mots on cartes.traduction = mots.id "
			+ "inner join langues on langues.id = mots.idLangue "
			+ "where langues.libelle != 'français' " );
		ResultSet res = st.executeQuery ();

		//LOOP THROUGH DATA AND ADD IT TO JSON
		while ( res.next () ) {
		    Map m = new LinkedHashMap ( 2 );
		    m.put ( "langue" , res.getString ( 1 ) );

		    PreparedStatement st2 = DataBase.getConnection ().prepareStatement ( "select distinct categories.libelle from cartes "
			    + "inner join cartes_categories ON  cartes_categories.idCarte = cartes.id "
			    + "inner join categories on cartes_categories.idCategorie = categories.id "
			    + "inner join mots on cartes.traduction = mots.id "
			    + "inner join langues on mots.idLangue = langues.id "
			    + "where langues.libelle= ? " );
		    st2.setString ( 1 , res.getString ( 1 ) );
		    ResultSet res2 = st2.executeQuery ();
		    //USE AN ARRAYLIST TO STORE THE CATEGORIES
		    List<String> s = new ArrayList<> ();
		    while ( res2.next () ) {
			s.add ( res2.getString ( 1 ) );
		    }
		    m.put ( "categorie(s)" , s );

		    arrJsonTemp.add ( m );
                    
                     //CLOSE SQL STATEMENT
                     st2.close();
                     res2.close();
		}
                
                 //CLOSE SQL STATEMENT
                st.close();
                res.close();

	    } catch ( Exception e ) {
		e.printStackTrace ();
	    }

	    //SQL STATUS
	    jsonObject.put ( "SQL_status" , "OK" );
	    //ADD DATA TO THE JSON OBJECT
	    jsonObject.put ( "data" , arrJsonTemp );
	    response.end ( jsonObject.encodePrettily () );

	} );

        
        /**--------------------
	 * ROUTE JEUX DE CARTES
	 ---------------------- */
	router.get ( "/api/jeuxdecartes/:langue/:categorie" ).handler ( ctx -> {

	    //Server HTTP
	    HttpServerResponse response = ctx.response ();
	    response.putHeader ( "Content-Type" , "application/json; charset=utf-8" );

	    //JSON utils
	    JsonObject jsonObject = new JsonObject ();
	    JSONArray arrJsonTemp = new JSONArray ();
	    JSONArray arrJsonTemp2 = new JSONArray ();

	    //PUT AUTORISATION ET TOKEN STATUS IN THE JSON OBJECT
	    jsonObject.put ( "authoristion_status" , ctx.data ().get ( "authoristion_status" ).toString () );
	    jsonObject.put ( "token_status" , ctx.data ().get ( "token_status" ).toString () );

	    //GET PARAMETRES IDUSER, LANGUE & CATEGORIE
	    String idUser = ctx.data ().get ( "idUser" ).toString ();
	    String langue = ctx.request ().getParam ( "langue" );
	    String categorie = ctx.request ().getParam ( "categorie" );

	    try {

		//CHECK IF THE LANGUAGE PARAMTER IS IN THE DB
		PreparedStatement st = DataBase.getConnection ().prepareStatement ( "select distinct langues.libelle from cartes "
			+ "inner join mots on cartes.traduction = mots.id "
			+ "inner join langues on langues.id = mots.idLangue  "
			+ "where langues.libelle != 'français' and langues.libelle= ?  " );
		st.setString ( 1 , langue );
		ResultSet res = st.executeQuery ();

                //IF THE LANGUAGE IS NOT IN THE DB -> ADD AN SQL STATUS ERROR
		if (  ! res.next () ) {
		    jsonObject.put ( "SQL_status" , "ERROR : no card matching language parameter" );
		    response.end ( jsonObject.encodePrettily () );
		} else {
		   
                    // CHEK IF CATEGORIE PARAMETER IS IN THE DB
		    PreparedStatement st2 = DataBase.getConnection ().prepareStatement ( "select distinct categories.libelle from cartes "
			    + "inner join cartes_categories ON  cartes_categories.idCarte = cartes.id "
			    + "inner join categories on cartes_categories.idCategorie = categories.id "
			    + "inner join mots on cartes.traduction = mots.id  "
			    + "inner join langues on mots.idLangue = langues.id "
			    + "where langues.libelle!='français' and langues.libelle= ? and categories.libelle= ? " );
		    st2.setString ( 1 , langue );
		    st2.setString ( 2 , categorie );
		    ResultSet res2 = st2.executeQuery ();
                    //IF THE CATEGORIE IS NOT IN THE DB -> ADD AN SQL STATUS ERROR
		    if (  ! res2.next () ) {
			jsonObject.put ( "SQL_status" , "ERROR : no card matching categorie parameter" );
			response.end ( jsonObject.encodePrettily () );

		    } else {
			// ORDER BY RANDOM DEPENDING OF THE SGBD IN USE
			String order = "by rand()";
			if ( DataBase.getSgdb () != "mariadb" ) {
			    order = "by random()";
			}
			//GET A RANDOMED SET OF 20 CARDS FROM COMPARTIMENT 5 AND LOWER
			PreparedStatement st3 = DataBase.getConnection ().prepareStatement ( "select idCarte, mot, traduction, categorie, langue, compartiment, userId "
				+ " from( "
				+ "select cartes.id as idCarte, mots.libelle as mot, "
				+ "traductions.libelle AS traduction, "
				+ "categories.libelle as categorie, "
				+ "langues.libelle AS langue,  "
				+ "compartiments.id AS compartiment, "
				+ "users.id As userId "
				+ "from cartes "
				+ "inner join mots on cartes.mot = mots.id "
				+ "inner join mots as traductions on cartes.traduction = traductions.id "
				+ "inner join langues on traductions.idLangue = langues.id "
				+ "inner join compartiments_users on compartiments_users.idCarte = cartes.id "
				+ "inner join compartiments on compartiments_users.idCompartiment = compartiments.id "
				+ "inner join cartes_categories ON cartes_categories.idCarte = cartes.id "
				+ "inner join categories on cartes_categories.idCategorie = categories.id "
				+ "inner join  users on compartiments_users.idUser = users.id "
				+ "where users.id=? AND langues.libelle=? AND categories.libelle=? "
				+ "order by compartiments.id DESC limit 20 ) "
				+ " as setOfCartesUser ORDER " + order + " " );
			st3.setInt ( 1 , Integer.parseInt ( idUser ) );
			st3.setString ( 2 , langue );
			st3.setString ( 3 , categorie );
			ResultSet res3 = st3.executeQuery ();

			//LOOP THROUGH DATA AND ADD IT TO JSON
			while ( res3.next () ) {
			    //CREATE HASHMAP TO ADD THE DATA TO THE JSON OBJECT
			    Map m = new LinkedHashMap ( 7 );
			    m.put ( "idCarte" , res3.getString ( 1 ) );
			    m.put ( "mot" , res3.getString ( 2 ) );
			    m.put ( "traduction" , res3.getString ( 3 ) );
			    m.put ( "categorie" , res3.getString ( 4 ) );
			    m.put ( "langue" , res3.getString ( 5 ) );
			    m.put ( "compartiment numéro" , res3.getString ( 6 ) );
			    m.put ( "userId" , res3.getString ( 7 ) );

			    arrJsonTemp.add ( m );

			}
			//SQL STATUS
			jsonObject.put ( "SQL_status" , "OK" );
			//ADD DATA TO THE JSON OBJECT
			jsonObject.put ( "data" , arrJsonTemp );
			response.end ( jsonObject.encodePrettily () );
                        
                        //CLOSE SQL STATEMENT
                        st3.close();
                        res3.close();

		    }
                    
                    //CLOSE SQL STATEMENT
                    st2.close();
                    res2.close();

		}
                
                //CLOSE SQL STATEMENT
                st.close();
                res.close();

	    } catch ( Exception e ) {
		e.printStackTrace ();
	    }

	} );

        
        /**-----------------
	 * ROUTE UPDATECARTE
	 ----------------- */
	router.post ( "/api/updatecarte" ).handler ( ctx -> {

	    //Server HTTP
	    HttpServerResponse response = ctx.response ();
	    response.putHeader ( "Content-Type" , "application/json; charset=utf-8" );

	    //JSON OBJECT FOR RESPONSE RESPONSE
	    JsonObject jsonObject = new JsonObject ();

	    //PUT AUTORISATION ET TOKEN STATUS IN THE JSON OBJECT
	    jsonObject.put ( "authoristion_status" , ctx.data ().get ( "authoristion_status" ).toString () );
	    jsonObject.put ( "token_status" , ctx.data ().get ( "token_status" ).toString () );

            // IF THE REQUEST DID NOT CONTAIN A JSON OBJECT WITH IDCARTE AND IDCATEGORIE -> SQL ERROR STATUS
	    if (  ! ctx.getBodyAsJson ().containsKey ( "idCarte" ) ||  ! ctx.getBodyAsJson ().containsKey ( "idCompartiment" ) ) {
		jsonObject.put ( "SQL_status" , "ERROR : one or more JSON data(s)( are missing or false" );
		response.end ( jsonObject.encodePrettily () );

	    } else {
		//IDCARTE, ID COMPARTIMENT AND IDUSER INITIALIZATION
		String idCarte = ctx.getBodyAsJson ().getString ( "idCarte" );
		String idCompartiment = ctx.getBodyAsJson ().getString ( "idCompartiment" );
		String idUser = ctx.data ().get ( "idUser" ).toString ();

		
                //CHECK THAT IDCARTE AND IDCOMPARTIMENT CONTAINS DIGITS ONLY
		if ( Utils.isDigit ( idCarte ) && Utils.isDigit ( idCompartiment ) ) {

		    try {

			//CHECK QUE L'ID DE LA CARTE SOIT BIEN DANS LES CARTES DU USER
                        //CHECK THAT THE IDCARTE IS IN THE SET OF CARDS OF THE USER
			PreparedStatement st = DataBase.getConnection ().prepareStatement ( "select compartiments_users.idCarte from compartiments_users "
				+ "inner join users on compartiments_users.idUser = users.id "
				+ "where users.id = ? and compartiments_users.idCarte = ? " );
			st.setInt ( 1 , Integer.parseInt ( idUser ) );
			st.setInt ( 2 , Integer.parseInt ( idCarte ) );
			ResultSet res = st.executeQuery ();

			//CHECK IDCOMPARTIMENT
			if ( res.next () ) {

			    //CHECK IDCOMPARTIMENT IS IN THE DB
			    PreparedStatement st2 = DataBase.getConnection ().prepareStatement ( "select compartiments.id from compartiments "
				    + "where compartiments.id= ? " );
			    st2.setInt ( 1 , Integer.parseInt ( idCompartiment ) );
			    ResultSet res2 = st2.executeQuery ();

			    //IF IDCARTE AND IDCOMPARTIMENT ARE CORRECT -> UPDATE
			    if ( res2.next () ) {

				//UPDATE CARD
				PreparedStatement st3 = DataBase.getConnection ().prepareStatement ( "UPDATE compartiments_users "
					+ "SET idCompartiment =  ? "
					+ "WHERE idCarte= ? and idUser= ? " );
				st3.setInt ( 1 , Integer.parseInt ( idCompartiment ) );
				st3.setInt ( 2 , Integer.parseInt ( idCarte ) );
				st3.setInt ( 3 , Integer.parseInt ( idUser ) );
				st3.executeUpdate ();
                                st3.close();

				jsonObject.put ( "SQL_status" , "OK" );
				response.end ( jsonObject.encodePrettily () );

			    } else {
				jsonObject.put ( "SQL_status" , "ERROR : unknown compartiment number" );
				response.end ( jsonObject.encodePrettily () );

			    }
                            
                            //CLOSE SQL STATEMENT
                            st2.close();
                            res2.close();
			} else {

			    jsonObject.put ( "SQL_status" , "ERROR : unknown card number" );
			    response.end ( jsonObject.encodePrettily () );
			}
                        
                         //CLOSE SQL STATEMENT
                        st.close();
                        res.close();

		    } catch ( Exception e ) {
			e.printStackTrace ();
		    }

		} else {

		    jsonObject.put ( "SQL_status" , "ERROR : wrong id format" );
		    response.end ( jsonObject.encodePrettily () );

		}

	    }

	} );
        
      
	/**-----------
	 * 404 ROUTER
	--------------*/
	router.route ().last ().handler ( ( RoutingContext context ) -> {
	    context.response ().setStatusCode ( 404 ).putHeader ( "content-type" , "text/plain" ).end ( "No matching data" );
	} );

    }

}
