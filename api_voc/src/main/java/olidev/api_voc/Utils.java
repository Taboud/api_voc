/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olidev.api_voc;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author lionel
 */
public class Utils {

    
    /**
    * ENCRYPTION SHA1
     * @param input
     * @return 
    */
    public static String sha1(String input) {
        String sha1 = null;
        try {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
            msdDigest.update(input.getBytes("UTF-8"), 0, input.length());
            sha1 = DatatypeConverter.printHexBinary(msdDigest.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace ();
        }
        return sha1.toLowerCase();
    }
    
    public static Boolean isDigit(String str){
        
        Boolean isDigit = true;
        char[] cs = str.toCharArray();
        for ( int i = 0; i< cs.length;i++){
           if ( ! Character.isDigit(cs[i])){
            isDigit = false;
            }
        }
        return isDigit;

    }

}
