/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olidev.api_voc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author lionel
 */
public class DataBase {

    /**-----------------
      MARIADB CONFIGURATION
     ----------------- */
    private static final String sgdb = "mariadb";
    private static final String port = "3306";
    private static final String dbName = "api_voc";
    private static final String url = "jdbc:mariadb://localhost";
    private static final String user = "api";
    private static final String passwd = "admin";
    private static final String driver = "org.mariadb.jdbc.Driver";

    /**-----------------
      POSTGRES CONFIGURATION
     ----------------- */
//    private static final String sgdb = "postgres";
//    private static final String port = "5432";
//    private static final String dbName = "api_voc";
//    private static final String url = "jdbc:postgresql://localhost";
//    private static final String user = "api";
//    private static final String passwd = "admin";
//    private static final String driver = "org.postgresql.Driver";
    
    /**-----------------
      CONNECTION CONFIGURATION
     ----------------- */
    private static final String connectionConfig = url + ":" + port + "/" + dbName;
    private static Connection connection = null;

    public static Connection getConnection () throws SQLException {

	if ( connection == null || !connection.isValid(180) || connection.isClosed()) {
	    connection = DriverManager.getConnection ( connectionConfig , user , passwd );
	}
	return connection;
    }

    public static String getSgdb () {
	return sgdb;
    }

}
